# File Comparison Extension for Nova

This runs FileMerge to compare two buffers.  To do this it copies the contents from the selected buffers to temporary files.
The files are called "NovaCompareFile_Left.tmp" and "NovaCompareFile_Right.tmp" and are stored in the "/tmp" directory.

## Requirements

Before using this extension, you must ensure that `Xcode` is installed on your system via the "App Store".

## Extension Installation

1. Open Nova.
2. Choose menu **Extensions** > **Extension Library...**
3. Search extension `FileDiff`
5. Click **Install**.

## Preferences 
**Executable Path**<br/>
`/Applications/Xcode.app/Contents/Applications/FileMerge.app/Contents/MacOS/FileMerge`<br/>
Manually enter or select the FileMerge binary location.

## Hotkeys
- To compare a buffer as the left file, use **^⌥<**
- To compare a buffer as the right file, use **^⌥>**
- To compare the selected buffers, use **^⌥?**