const DEFAULT_CMD = '/Applications/Xcode.app/Contents/Applications/FileMerge.app/Contents/MacOS/FileMerge';

exports.activate = function() {
    // Do work when the extension is activated
}

exports.deactivate = function() {
    // Clean up state before the extension is deactivated
}

nova.commands.register("filediff.compareFiles", (workspace) => {
    console.log('Comparing files...');
    var cmd = nova.config.get('filediff.toolpath','string');
    if(cmd == null) cmd = DEFAULT_CMD
    if(cmd != null) {
        var options = {
            args: ['-left','/tmp/NovaCompareFile_Left.tmp','-right','/tmp/NovaCompareFile_Right.tmp']
        };
        var out = [];
                
        var process = new Process(cmd,options);
            
        process.onStdout(function(data) {
            if(data) {
                out.push(data);
                console.log(data);
            }
        });
        process.onStderr(function(data) {
            if(data) console.log(data);
        });
        process.onDidExit(function(status) {
            //var string = "External Tool Exited with Stdout:\n" + out.join("");
            //nova.workspace.showInformativeMessage(string);
        });
        process.start();
    } else {
        nova.workspace.showInformativeMessage('No file path has been specified.');
    }
});

nova.commands.register("filediff.setLeftFile", (workspace) => {
    var options = {
        "placeholder": "/path/to/tool",
        "prompt": "Run"
    };
    var te = nova.workspace.activeTextEditor;
    var c = te.getTextInRange(new Range(0,te.document.length));
    
    console.log('Writing left file...');
    
    var f = nova.fs.open('/tmp/NovaCompareFile_Left.tmp','w');
    f.write(c);
});

nova.commands.register("filediff.setRightFile", (workspace) => {
    var options = {
        "placeholder": "/path/to/tool",
        "prompt": "Run"
    };
    var te = nova.workspace.activeTextEditor;
    var c = te.getTextInRange(new Range(0,te.document.length));
    
    console.log('Writing right file...');
        
    var f = nova.fs.open('/tmp/NovaCompareFile_Right.tmp','w');
    f.write(c);
});

nova.config.onDidChange('filediff.toolpath', function(newValue, oldValue) {
     cmd = newValue;
     console.log('Changed cmd to "'+cmd+'"');
}, this);
